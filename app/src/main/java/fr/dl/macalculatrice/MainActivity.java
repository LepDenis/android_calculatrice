package fr.dl.macalculatrice;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import fr.dl.macalculatrice.metier.Calculatrice;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView resultat;
    private Calculatrice maCalculatrice = new Calculatrice();

    // Initialisation de l'activité
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button bouton1 = findViewById(R.id.bouton1);
        bouton1.setOnClickListener(this);

        Button bouton2 = findViewById(R.id.bouton2);
        bouton2.setOnClickListener(this);

        Button bouton3 = findViewById(R.id.bouton3);
        bouton3.setOnClickListener(this);

        Button bouton4 = findViewById(R.id.bouton4);
        bouton4.setOnClickListener(this);

        Button bouton5 = findViewById(R.id.bouton5);
        bouton5.setOnClickListener(this);

        Button bouton6 = findViewById(R.id.bouton6);
        bouton6.setOnClickListener(this);

        Button bouton7 = findViewById(R.id.bouton7);
        bouton7.setOnClickListener(this);

        Button bouton8 = findViewById(R.id.bouton8);
        bouton8.setOnClickListener(this);

        Button bouton9 = findViewById(R.id.bouton9);
        bouton9.setOnClickListener(this);

        Button bouton0 = findViewById(R.id.bouton0);
        bouton0.setOnClickListener(this);

        Button boutonPoint = findViewById(R.id.boutonPoint);
        boutonPoint.setOnClickListener(this);

        Button boutonAddition = findViewById(R.id.boutonAddition);
        boutonAddition.setOnClickListener(this);

        Button boutonSoustraction = findViewById(R.id.boutonSoustraction);
        boutonSoustraction.setOnClickListener(this);

        Button boutonMultiplication = findViewById(R.id.boutonMultiplication);
        boutonMultiplication.setOnClickListener(this);

        Button boutonDivision = findViewById(R.id.boutonDivision);
        boutonDivision.setOnClickListener(this);

        Button boutonEgal = findViewById(R.id.boutonEgal);
        boutonEgal.setOnClickListener(this);

        Button boutonC = findViewById(R.id.boutonReinitialize);
        boutonC.setOnClickListener(this);

        Button boutonSM = findViewById(R.id.boutonSetMemory);
        boutonSM.setOnClickListener(this);

        Button boutonM = findViewById(R.id.boutonCallMemory);
        boutonM.setOnClickListener(this);

        Button boutonRM = findViewById(R.id.boutonResetMemory);
        boutonRM.setOnClickListener(this);

        Button boutonSigne = findViewById(R.id.boutonSigne);
        boutonSigne.setOnClickListener(this);

        Button boutonPuissance = findViewById(R.id.boutonPuissance);
        boutonPuissance.setOnClickListener(this);

        Button boutonRetour = findViewById(R.id.boutonRetour);
        boutonRetour.setOnClickListener(this);

        Button boutonInverse = findViewById(R.id.boutonInverse);
        boutonInverse.setOnClickListener(this);

        resultat = findViewById(R.id.textViewResultat);

        onClick(bouton0);
    }

    // Envoie les informations à la Calculatrice
    @Override
    public void onClick(View view) {

        Button btn = view.findViewById(view.getId());
        resultat.setText(maCalculatrice.presserTouche(btn.getText().toString()));
    }

    // Sauvegarder de l'etat de l'activité
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString("savenombre1", maCalculatrice.getNombre1());
        savedInstanceState.putString("savenombre2", maCalculatrice.getNombre2());
        savedInstanceState.putString("saveoperateur", maCalculatrice.getOperateur());
        savedInstanceState.putString("saveresultat", maCalculatrice.getResultat());
        savedInstanceState.putString("savememory", maCalculatrice.getMemory());
        savedInstanceState.putString("saveoperation", maCalculatrice.getOperation());
        savedInstanceState.putBoolean("saveisoperateur", maCalculatrice.isOperateur());
        savedInstanceState.putBoolean("saveisvirgule1", maCalculatrice.isVirgule1());
        savedInstanceState.putBoolean("saveisvirgule2", maCalculatrice.isVirgule2());
    }

    // Restauration de l'état de l'activité
    public void onRestoreInstanceState(Bundle savedInstanceState) {

        super.onRestoreInstanceState(savedInstanceState);
        maCalculatrice.setNombre1(savedInstanceState.getString("savenombre1"));
        maCalculatrice.setNombre2(savedInstanceState.getString("savenombre2"));
        maCalculatrice.setOperateur(savedInstanceState.getString("saveoperateur"));
        maCalculatrice.setResultat(savedInstanceState.getString("saveresultat"));
        maCalculatrice.setMemory(savedInstanceState.getString("savememory"));
        maCalculatrice.setOperation(savedInstanceState.getString("saveoperation"));
        maCalculatrice.setIsOperateur(savedInstanceState.getBoolean("saveisoperateur"));
        maCalculatrice.setVirgule1(savedInstanceState.getBoolean("saveisvirgule1"));
        maCalculatrice.setVirgule2(savedInstanceState.getBoolean("saveisvirgule2"));

        if (!maCalculatrice.getOperation().equals(""))
            resultat.setText(maCalculatrice.getOperation());
        else
            maCalculatrice.presserTouche("0");
    }
}
