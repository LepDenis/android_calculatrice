package fr.dl.macalculatrice.metier;

import java.math.BigDecimal;
import java.math.MathContext;

public class Calculatrice {

    private String nombre1;
    private String nombre2;
    private String operateur;
    private String resultat;
    private String memory;
    private String operation;
    private boolean isOperateur;
    private boolean isVirgule1;
    private boolean isVirgule2;

    // Constructeur
    public Calculatrice() {

        nombre1 = "";
        nombre2 = "";
        operateur = "";
        resultat = "";
        memory = "0";
        operation = "";
        isOperateur = false;
        isVirgule1 = false;
        isVirgule2 = false;
    }

    // Gère de le traitement des informations lorque l'on appuie sur une touche du clavier
    public String presserTouche(String chaine) {
        switch (chaine) {
            case ".":
                presserPoint(chaine);
                break;
            case "+ -":
                changerSigne();
                break;
            case "x²":
                calculerCarre();
                break;
            case "1/x":
                inverse();
                break;
            case "←":
                effacer();
                break;
            case "C":
                reinitialiser();
                nombre1 = "0";
                break;
            case "SM":
                storeMemory();
                break;
            case "M":
                callMemory();
                break;
            case "RM":
                memory = "0";
                break;
            case "+":
            case "-":
            case "X":
            case "/":
                presserOperateur(chaine);
                break;
            case "=":
                if (!nombre2.equals(""))
                    calculer();
                break;
            default:
                presserChiffre(chaine);
                break;
        }
        operation = nombre1 + ((isOperateur) ? (" " + operateur) : (""))
                + ((!nombre2.equals("")) ? (" " + nombre2) : (""));
        return operation;
    }

    // Méthode dans le cas où l'on presse un chiffre
    private void presserChiffre(String chaine) {
        if (!isOperateur) {
            if (nombre1.equals("0") || nombre1.equals(resultat) || nombre1.equals("ERROR"))
                nombre1 = "";
            nombre1 += chaine;
        } else {
            if (nombre2.equals("0") || nombre2.equals(memory))
                nombre2 = "";
            nombre2 += chaine;
        }
    }

    // Méthode dans le cas où l'on presse la virgule
    private void presserPoint(String chaine) {
        if (nombre1.equals("ERROR"))
            nombre1 = "0";
        if (!nombre1.equals(""))
            checkVirgule1();
        if (!isOperateur) {
            if (nombre1.equals(resultat)) {
                nombre1 = "0";
                nombre1 += chaine;
                isVirgule1 = true;
            }
            if (!isVirgule1) {
                nombre1 += chaine;
                isVirgule1 = true;
            }
        } else {
            if (!isVirgule2) {
                if (nombre2.equals(""))
                    nombre2 += "0";
                nombre2 += chaine;
                isVirgule2 = true;
            }
        }
    }

    // Méthode dans le cas où l'on presse un opérateur arithmétique
    private void presserOperateur(String chaine) {
        if (!isOperateur) {
            if (nombre1.equals("ERROR"))
                nombre1 = "0";
            if (chaine.equals("X"))
                operateur = "*";
            else
                operateur = chaine;
            nombre1 = retirerZeroEtVirguleInutiles(nombre1);
            isOperateur = true;
        } else {
            if (nombre2.equals("")) {
                if (chaine.equals("X"))
                    operateur = "*";
                else
                    operateur = chaine;
            } else {
                calculer();
                isOperateur = true;
                if (chaine.equals("X"))
                    operateur = "*";
                else
                    operateur = chaine;
            }
        }
    }

    // Méthode dans le cas où l'on appuie sur la touche "SM"
    private void storeMemory() {
        if (!nombre1.equals("ERROR") && !nombre2.equals(""))
            calculer();
        if (nombre1.equals("ERROR"))
            memory = "0";
        else
            memory = nombre1;

    }

    // Méthode dans le cas où l'on presse la touche "M"
    private void callMemory() {
        if (!isOperateur) {
            reinitialiser();
            nombre1 = memory;
        } else
            nombre2 = memory;
    }

    // Méthode dans le cas où l'on presse la touche "+ -"
    private void changerSigne() {
        if (!isOperateur) {
            if (!nombre1.equals("") && !nombre1.equals("0") && !(nombre1.charAt(0) == '-') && !nombre1.equals("ERROR"))
                nombre1 = "-" + nombre1;
            else if (nombre1.charAt(0) == '-')
                nombre1 = nombre1.substring(1, nombre1.length());
        } else {
            if (!nombre2.equals("")) {
                if (!nombre2.equals("0") && !(nombre2.charAt(0) == '-'))
                    nombre2 = "-" + nombre2;
                else {
                    if (nombre2.charAt(0) == '-')
                        nombre2 = nombre2.substring(1, nombre2.length());
                }
            } else if (nombre1.charAt(0) == '-')
                nombre1 = nombre1.substring(1, nombre1.length());
            else if (!nombre1.equals("ERROR") && !nombre1.equals("0"))
                nombre1 = "-" + nombre1;
        }
    }

    // Méthode dans le cas où l'on presse la touche "1/x"
    private void inverse() {
        if (!nombre2.equals(""))
            calculer();
        if (!nombre1.equals("ERROR")) {
            nombre2 = nombre1;
            nombre1 = "1";
            operateur = "/";
            isOperateur = true;
            calculer();
        } else
            nombre1 = "0";
    }

    // Méthode dans le cas où l'on appuie sur la touche "x²"
    private void calculerCarre() {
        if (!nombre2.equals(""))
            calculer();
        operateur = "^";
        isOperateur = true;
        nombre2 = "0";
        calculer();
    }

    // Méthode dans le cas où l'on appuie sur la touche "←"
    private void effacer() {
        if (!nombre1.equals("ERROR")) {
            if (!isOperateur && !nombre1.equals("0"))
                nombre1 = nombre1.substring(0, nombre1.length() - 1);
            if (isOperateur && nombre2.equals("")) {
                operateur = "";
                isOperateur = false;
            }
            if (isOperateur && !nombre2.equals(""))
                nombre2 = nombre2.substring(0, nombre2.length() - 1);
        }
        if (nombre1.length() == 0 || nombre1.equals("-"))
            nombre1 = "0";
        if (nombre2.equals("-"))
            nombre2 = "";
    }

    // Vérifie si le nombre1 possède une virgule
    private void checkVirgule1() {
        int i = 0;
        do {
            if (nombre1.charAt(i) == '.')
                isVirgule1 = true;
            i++;
        } while (!isVirgule1 && i < nombre1.length());
    }

    // Effectue les calculs
    private void calculer() {
        if (nombre1.equals("ERROR") || nombre1.trim().equals("-"))
            nombre1 = "0";
        if (nombre2.trim().equals("-"))
            nombre2 = "0";
        BigDecimal bd1 = new BigDecimal(nombre1);
        BigDecimal bd2 = new BigDecimal(nombre2);
        BigDecimal resultatArithmetique = new BigDecimal(0);
        switch (operateur) {
            case "+":
                resultatArithmetique = bd1.add(bd2);
                break;
            case "-":
                resultatArithmetique = bd1.subtract(bd2);
                break;
            case "*":
                resultatArithmetique = bd1.multiply(bd2);
                break;
            case "/":
                if (bd2.compareTo(BigDecimal.ZERO) == 0)
                    resultat = "ERROR";
                else
                    resultatArithmetique = bd1.divide(bd2, new MathContext(20));
                break;
            case "^":
                resultatArithmetique = bd1.pow(2, new MathContext(20));
                break;
        }
        if (!resultat.equals("ERROR"))
            resultat = resultatArithmetique.toString();
        resultat = retirerZeroEtVirguleInutiles(resultat);
        nombre1 = resultat;
        reinitialiser();
    }

    // Permet de retirer les zéros non significatifs et une virgule inutile
    private String retirerZeroEtVirguleInutiles(String chaine) {
        while (!chaine.equals("0") && (chaine.charAt(chaine.length() - 1) == '0'
                || chaine.charAt(chaine.length() - 1) == '.'))
            chaine = chaine.substring(0, (chaine.length() - 1));
        return chaine;
    }

    // Réinitialise les variables à l'état de départ
    private void reinitialiser() {
        isVirgule1 = false;
        isVirgule2 = false;
        isOperateur = false;
        nombre2 = "";
        operateur = "";
        if (resultat.equals("ERROR"))
            resultat = "";
    }


    // Accesseurs
    public String getNombre1() {
        return nombre1;
    }

    public String getNombre2() {
        return nombre2;
    }

    public String getOperateur() {
        return operateur;
    }

    public String getResultat() {
        return resultat;
    }

    public String getMemory() {
        return memory;
    }

    public String getOperation() {
        return operation;
    }

    public boolean isOperateur() {
        return isOperateur;
    }

    public boolean isVirgule1() {
        return isVirgule1;
    }

    public boolean isVirgule2() {
        return isVirgule2;
    }

    public void setNombre1(String nombre1) {
        this.nombre1 = nombre1;
    }

    public void setNombre2(String nombre2) {
        this.nombre2 = nombre2;
    }

    public void setOperateur(String operateur) {
        this.operateur = operateur;
    }

    public void setResultat(String resultat) {
        this.resultat = resultat;
    }

    public void setMemory(String memory) {
        this.memory = memory;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public void setIsOperateur(boolean operateur) {
        isOperateur = operateur;
    }

    public void setVirgule1(boolean virgule1) {
        isVirgule1 = virgule1;
    }

    public void setVirgule2(boolean virgule2) {
        isVirgule2 = virgule2;
    }

}
